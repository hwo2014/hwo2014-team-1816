<?php

class NoobBot extends BasicBot {
	
	var $myTickCounter = 0;
	var $gameStarted = false;
	var $throttle = 0.62;

	function __construct($host, $port, $botname, $botkey, $debug = FALSE) {
    $debug = true;
    parent::__construct($host, $port, $botname, $botkey, $debug);
	}

	public function run() {
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {
				case 'carPositions':
					//$this->throttle += 0.01;
					$this->throttle = $this->throttle > 0.64 ? 0.64 : $this->throttle;
					$this->write_msg('throttle', $this->throttle);
					$this->debug('throttle: ' . $this->throttle);
					break;
				case 'join':
				case 'yourCar':
				case 'gameInit':
				case 'gameStart':
					$this->gameStarted = true;
					$this->write_msg('switchLane', 'Right');
				case 'crash':
				case 'spawn':
				case 'lapFinished':
				case 'dnf':
				case 'finish':
				default:
					$this->write_msg('ping', null);
			}
		}
    if($this->gameStarted){
      $this->myTickCounter++;
    }
		
	}
}
?>