#!/usr/bin/php
<?php
require_once 'BasicBot.class.php';
require_once 'NoobBot/NoobBot.class.php';

if (count($argv) < 5) {
	die("Usage: bot host port botname botkey\n");
}
try {
	$bot = new NoobBot($argv[1], $argv[2], $argv[3], $argv[4]);
} catch (Exception $e) {
	die($e->getMessage() . "\n");
}
$bot->run();
?>


